import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {MoviesModel} from "../models/movies.model";


@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(
    private http: HttpClient,
  ) {
  }

  baseUrl: string = 'http://localhost:3015/api/';

  getAllMovies$(): Observable<MoviesModel[]> {
    return this.http.get<MoviesModel[]>(this.baseUrl + 'actors');
  }

  postMovies$(movie: MoviesModel) {
    return this.http.post(this.baseUrl + 'actor', movie);
  }

  getMoviesById$(id: string): Observable<MoviesModel> {
    return this.http.get<MoviesModel>(this.baseUrl + id);
  }

  deleteActor$(id: string) {
    return this.http.delete(this.baseUrl + 'actors' + '/' + id);
  }

  putMovie$(movie: MoviesModel, id: number) {
    return this.http.put(this.baseUrl + 'actors' + '/' + movie.id, movie);
  }
}
