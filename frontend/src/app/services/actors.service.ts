import {Injectable} from '@angular/core';
import {ActorModel} from "../models/actor.model";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class ActorsService {

  constructor(
    private http: HttpClient,
  ) {
  }

  baseUrl: string = 'http://localhost:3015/api/';

  getAllActors$(): Observable<ActorModel[]> {
    return this.http.get<ActorModel[]>(this.baseUrl + 'actors');
  }

  postActor$(actor: ActorModel): Observable<ActorModel> {
    return this.http.post<ActorModel>(this.baseUrl + 'actor', actor);
  }

  deleteActor$(id: number) {
    return this.http.delete(this.baseUrl + 'actors' + '/' + id);
  }

  putActor(actor: ActorModel, id: number) {
    return this.http.put<ActorModel>(this.baseUrl + 'actors' + '/' + actor.id, actor);
  }
}





