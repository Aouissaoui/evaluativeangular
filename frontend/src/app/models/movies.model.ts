import {Timestamp} from 'rxjs';

export interface  MoviesModel {
  id: number,
  title: string,
  category: string,
  releaseYear: string,
  poster: string,
  directors: string,
  actors: number[],
  synopsis: string,
  rate: number,
  lastViewDate: Timestamp<any>,
  price: number,
}

