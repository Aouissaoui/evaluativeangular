import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainContentComponent} from './main-content.component';

const routes: Routes = [
  {

    path: '',
    component: MainContentComponent,
    children: [

      {
        path: '',
        loadChildren: () => import('../home/home.module').then(m => m.HomeModule),
      },

      {
        path: 'list-movies',
        loadChildren: () => import('../movie-list/film-list.module').then(m => m.FilmListModule),
      },

      {
        path: 'list-actors',
        loadChildren: () => import('../actors-list/actos-list.module').then(m => m.ActosListModule),
      },

      {
        path: 'create-actor',
        loadChildren: () => import('../actors-list/create-actor/create-actor.module').then(m => m.CreateActorModule),
      },
      {
        path: 'create-movie',
        loadChildren: () => import('../movie-list/create-movie/create-movie.module').then(m => m.CreateMovieModule),
      },

      {
        path: 'edit-actor/:id',
        loadChildren: () => import('../actors-list/edit-actor/edit-actor.module').then(m => m.EditActorModule),
      },

      {
        path: 'details-movie/:id',
        loadChildren: () => import('../movie-list/detail-movie/detail-film.module').then(m => m.DetailFilmModule),
      },

    ]
  },
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MainContentRoutingModule {
}
