import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ActorsListRoutingModule} from "./actors-list.routing.module";
import {ActorsListComponent} from "./actors-list.component";
import {NgZorroAntdModule} from "ng-zorro-antd";
import {MatIconModule} from "@angular/material/icon";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";


@NgModule({
  declarations: [
    ActorsListComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ActorsListRoutingModule,
    NgZorroAntdModule,
    MatIconModule,
    MatPaginatorModule,
    MatTableModule

  ]
})

export class ActosListModule {
}
