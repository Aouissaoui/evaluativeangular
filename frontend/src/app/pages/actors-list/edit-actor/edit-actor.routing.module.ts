import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EditActorComponent} from "./edit-actor.component";


const routes: Routes = [
  {
    path: '',
    component: EditActorComponent,
  },

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditActorRoutingModule {
}
