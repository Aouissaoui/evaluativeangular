import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActorsService} from "../../../services/actors.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-edit-actor',
  templateUrl: './edit-actor.component.html',
  styleUrls: ['./edit-actor.component.scss']
})
export class EditActorComponent implements OnInit {
  ActorForm: FormGroup;

  constructor(private fb: FormBuilder,
              private  actorsservice: ActorsService,
              private router: Router,) {
  }

  ngOnInit(): void {
    this.initForm()
  }


  initForm() {
    this.ActorForm = this.fb.group({
      name: ['actor? actor.name', [Validators.required]],
    });
  }

  postactor() {
    this.actorsservice.postActor$(this.ActorForm.value)
  }

  submitForm() {
    this.postactor();
  }
}
