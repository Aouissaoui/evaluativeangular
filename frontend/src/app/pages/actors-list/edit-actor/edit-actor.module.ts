import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditActorComponent} from "./edit-actor.component";
import {EditActorRoutingModule} from "./edit-actor.routing.module";
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [EditActorComponent],
  exports: [],
  imports: [
    CommonModule,
    EditActorRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule


  ]
})
export class EditActorModule {
}
