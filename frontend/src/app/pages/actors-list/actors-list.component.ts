import {Component, OnInit, ViewChild} from '@angular/core';
import {ActorModel} from "../../models/actor.model";
import {tap} from "rxjs/operators";
import {ActorsService} from "../../services/actors.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {Router} from "@angular/router";

@Component({
  selector: 'app-actors-list',
  templateUrl: './actors-list.component.html',
  styleUrls: ['./actors-list.component.scss']
})
export class ActorsListComponent implements OnInit {
  actors: ActorModel[];
  datasource = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'name', 'visibility'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private  actorsservice: ActorsService,
              private router: Router) {
  }

  ngOnInit(): void {

    this.actorsservice.getAllActors$()
      .pipe(
        tap((actors: ActorModel[]) => this.datasource.data = actors)
      )
      .subscribe();

  }

  deleteActors(actor: ActorModel) {
    this.actorsservice.deleteActor$(actor.id)
      .subscribe()
  }

  goToEditActor(actor: ActorModel) {
    return this.router.navigate(['/edit-actor', actor.id])
  }

}
