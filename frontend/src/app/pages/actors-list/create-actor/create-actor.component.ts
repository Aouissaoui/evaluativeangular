import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActorsService} from "../../../services/actors.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-actor',
  templateUrl: './create-actor.component.html',
  styleUrls: ['./create-actor.component.scss']
})
export class CreateActorComponent implements OnInit {

  ActorForm: FormGroup;

  constructor(private fb: FormBuilder,
              private  actorsservice: ActorsService,
              private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.initForm();
  }


 private initForm() {
    this.ActorForm = this.fb.group({
      name: ['actor? actor.name', [Validators.required]],
    });
  }

  postactor() {
    this.actorsservice.postActor$(this.ActorForm.value)
  }

  submitForm() {
    this.postactor();
  }

  cancelActorsCreate() {
    return this.router.navigate(['/', 'actors']);
  }
}
