import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateActorComponent} from './create-actor.component';


const routes: Routes = [
  {
    path: '',
    component: CreateActorComponent,
  },

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateActorRoutingModule {
}
