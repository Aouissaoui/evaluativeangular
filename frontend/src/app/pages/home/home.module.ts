import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home.routing.module';
import {
  NzButtonModule,
  NzCardModule,
  NzGridModule, NzIconModule,
  NzLayoutModule,
  NzSliderModule,
  NzTypographyModule
} from 'ng-zorro-antd';
import {FormsModule} from '@angular/forms';
import {MatMenuModule} from "@angular/material/menu";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    NzButtonModule,
    NzCardModule,
    NzTypographyModule,
    NzGridModule,
    NzSliderModule,
    FormsModule,
    NzLayoutModule,
    NzIconModule,
    MatMenuModule,
    MatButtonModule,
  ]
})

export class HomeModule {
}
