import {Component, OnInit} from '@angular/core';
import {MoviesService} from "../../../services/movies.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MoviesModel} from "../../../models/movies.model";
import {ActivatedRoute, Router} from "@angular/router";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-detail-film',
  templateUrl: './detail-film.component.html',
  styleUrls: ['./detail-film.component.scss']
})
export class DetailFilmComponent implements OnInit {
  movieForm: FormGroup;
  movie: MoviesModel;

  constructor(private moviesservices: MoviesService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
  ) {
  }
  ngOnInit(): void {
    this.private_initForm();
    this.getMoviesById()
  }

  submitForm(data) {

    this.putMovie(data);

  }

  putMovie(actor) {
    this.moviesservices.putMovie$(actor, this.movie.id)
      .pipe(

      )
      .subscribe()
  }

  private_initForm() {
    this.movieForm = this.fb.group({
      title: ['movie? movie.title', [Validators.required]],
      category: ['movie? movie.category', [Validators.required]],
      releaseYear: ['movie? movie.releaseYear', [Validators.required]],
      poster: ['movie? movie.poster', [Validators.required]],
      directors: ['movie? movie.directors', [Validators.required]],
      actors: ['movie? movie.actors', [Validators.required]],
      synopsis: ['movie? movie.synopsis', [Validators.required]],
      rate: ['movie? movie.rate', [Validators.required]],
      lastViewDate: ['movie? movie.lastViewDate', [Validators.required]],
      price: ['movie? movie.price', [Validators.required]],


    });
  }

  getMoviesById() {
    this.moviesservices.getMoviesById$(this.route.snapshot.params.id)
      .pipe(
        tap(movie => this.movie = movie as MoviesModel),
        tap(() => this.initForm(this.movie))
      )
      .subscribe()
  }

  cancelMoviesDetail() {
    return this.router.navigate(['/home', 'list-movies']);
  }
}
