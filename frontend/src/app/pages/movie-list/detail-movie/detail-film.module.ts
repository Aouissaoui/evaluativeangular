import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DetailFilmComponent} from './detail-film.component';

@NgModule({
  declarations: [DetailFilmComponent],
  exports: [],
  imports: [
    CommonModule,

  ]
})
export class DetailFilmModule {
}
