import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DetailFilmComponent} from './detail-film.component';
import {MatCardModule} from "@angular/material/card";
import {MatDividerModule} from "@angular/material/divider";
import {MatFormFieldModule} from "@angular/material/form-field";

@NgModule({
  declarations: [DetailFilmComponent],
  exports: [],
  imports: [
    CommonModule,
    MatCardModule,
    MatDividerModule,
    MatFormFieldModule,

  ]
})
export class DetailFilmRoutingModule {
}
