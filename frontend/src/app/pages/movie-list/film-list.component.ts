import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {MoviesModel} from "../../models/movies.model";
import {tap} from "rxjs/operators";
import {MoviesService} from "../../services/movies.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.scss']
})
export class FilmListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  datasource2 = new MatTableDataSource();
  displayedColumns2: string[] = ['id', 'title', 'category', 'releaseYear', 'poster', 'directors', 'synopsis', 'rate', 'lastViewDate', 'price'];

  constructor(private moviesservices: MoviesService,
              private router: Router) {
  }


  ngOnInit(): void {

    this.datasource2.paginator = this.paginator;
    this.moviesservices.getAllMovies$()
      .pipe(
        tap((movies: MoviesModel[]) => this.datasource2.data = movies)
      )
      .subscribe();

  }

  goToMoviesDetail(movie: MoviesModel) {
    return this.router.navigate(['/details-movie', movie.id])

  }


}


