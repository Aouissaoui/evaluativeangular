import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilmListComponent} from './film-list.component';
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [FilmListComponent],
  exports: [],
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,

  ]
})
export class FilmListModule {
}
