import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateMovieComponent} from './create-movie.component';


const routes: Routes = [
  {
    path: '',
    component: CreateMovieComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateMovieRoutingModule {
}
