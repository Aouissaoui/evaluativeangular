import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MoviesModel} from "../../../models/movies.model";
import {MoviesService} from "../../../services/movies.service";

@Component({
  selector: 'app-create-movie',
  templateUrl: './create-movie.component.html',
  styleUrls: ['./create-move.component.scss']
})
export class CreateMovieComponent implements OnInit {

  movieForm: FormGroup;
  movie: MoviesModel;

  constructor(private moviesservices: MoviesService,
              private fb: FormBuilder,
  ) {
  }

  ngOnInit(): void {
    this._initForm()
  }

  private _initForm() {

    this.movieForm = this.fb.group({
      title: ['movie? movie.title', [Validators.required]],
      category: ['movie? movie.category', [Validators.required]],
      releaseYear: ['movie? movie.releaseYear', [Validators.required]],
      poster: ['movie? movie.poster', [Validators.required]],
      directors: ['movie? movie.directors', [Validators.required]],
      actors: ['movie? movie.actors', [Validators.required]],
      synopsis: ['movie? movie.synopsis', [Validators.required]],
      rate: ['movie? movie.rate', [Validators.required]],
      lastViewDate: ['movie? movie.lastViewDate', [Validators.required]],
      price: ['movie? movie.price', [Validators.required]],


    });
  }

  submitForm() {

    this.postmovie();

  }


  postmovie() {
    this.moviesservices.postMovies$(this.movieForm.value)
  }
}

